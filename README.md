# Call center system #

Este sistema administra y controla un sistema de llamadas de un call center.

### Componentes ###

* _**CallCenter:** Es la clase principal del sistema. En ella se inicializa el sistema y todos sus componentes_
* _**CallEnqueuer:** Sirve para manejar la cola de las llamadas que estan en espera_
* _**AvailabilityManager:** Sirve para manejar la cola de empleados disponibles_
* _**Dispatcher:** Sirve para manejar el flujo de llamadas, derivandola a un operador o poniendolas en espera_

### Componentes auxiliares ###

* _**Caller:** Simula una persona que llama al callcenter_
* _**CallManager:** Sirve para administrar la simulaci�n de llamadas al callcenter. Con ella se inicializa el batch de llamadas (Entrada al sistema)_
* _**EmployeeActivityMonitor:** Monitorea la actividad de los empleados y maneja su comportamiento aplicando las acciones respectivas_


### Como funciona ###

* _El call manager genera llamadas que las env�a al call center_
* _El Call center se encarga de asignarlas al dispatcher_
* _El dispatcher solicitar� un asesor disponible al availability manager_
* _El availability manager retornar� el primer asesor disponible basado en la prioridad (1 -> operador, 2 -> supervisor, 3 -> director)_
* _Si no hay asesores disponibles para atender, el dispatcher enviar� la llamada a la cola de espera (call enqueuer)_
* _Cuando el asesor contesta la llamada, un proceso autom�tico en el employee activity monitor cortar� la llamada dentro de los 5-10 segundos de atendida la misma_
* _El employee activity monitor estar� monitoreando la llamada, cuando esta finalice, se le notificar� al dispatcher que hay un nuevo asesor disponible_
* _Cuando el dispatcher recibe la notificaci�n de que un asesor se encuentra nuevamente disponible le avisa al availability manager para que lo vuelva a poner en la cola de atenci�n y agarra la primer llamada en espera para despacharla_

### Como utilizarlo ###

* Ejecutar la clase CallManager para incializar el batch de llamadas

### Contacto ###

* [Mail](mailto:juan.furattini@gmail.com)
* [LinkedIn](https://www.linkedin.com/in/furattinijuan/)

-----------------------

*Juan Furattini*