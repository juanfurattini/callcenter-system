package com.almundo.callcenter.observers;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.almundo.callcenter.components.Dispatcher;
import com.almundo.callcenter.models.call.PhoneCall;
import com.almundo.callcenter.models.employee.Employee;

/**
 * EmployeeActivityMonitor
 * 
 * This class monitors the activity of the employees and handle their behavior
 * by applying the respective actions
 * 
 * @author juan.m.furattini
 *
 */
public class EmployeeActivityMonitor implements Observer {

	private static final int MIN_CALL_DURATION = 5000;
	private static final int MAX_CALL_DURATION = 10000;
	private static final int CALL_DURATION_FACTOR = 1000;

	// Singleton instance
	private static EmployeeActivityMonitor _instance = new EmployeeActivityMonitor();

	// Components
	private Dispatcher dispatcher = Dispatcher.getInstance();

	private EmployeeActivityMonitor() {
	}

	public static EmployeeActivityMonitor getInstance() {
		return _instance;
	}

	@Override
	public void update(Observable o, Object arg) {
		Employee attender = (Employee) o;
		PhoneCall call = (PhoneCall) arg;
		switch (attender.getStatus()) {
		case AVAILABLE:
			notifyEmployeeReleased(attender);
			break;
		case ATTENDING:
			scheduleEndOfCall(attender, call);
			break;
		default:
			break;
		}
	}

	/**
	 * Notifies the dispatcher that an employee has been released
	 * 
	 * @param attender
	 */
	private void notifyEmployeeReleased(Employee attender) {
		dispatcher.employeeReleased(attender);
	}

	/**
	 * Schedules the end of a call
	 * 
	 * @param attender
	 * @param call
	 */
	private void scheduleEndOfCall(final Employee attender, final PhoneCall call) {
		int callTime = calculateTimeToFinishCall();
		ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(1);
		scheduledThreadPool.schedule(new Runnable() {
			@Override
			public void run() {
				attender.finishCall(call);
			}
		}, callTime, TimeUnit.MILLISECONDS);
		scheduledThreadPool.shutdown();
	}

	/**
	 * Calculates a random time between MIN_CALL_DURATION and MAX_CALL_DURATION
	 * to finish the call in milliseconds
	 * 
	 * @return a random time to finish the call in seconds
	 */
	public int calculateTimeToFinishCall() {
		return (new Random().nextInt((MAX_CALL_DURATION - MIN_CALL_DURATION) + CALL_DURATION_FACTOR)
				+ MIN_CALL_DURATION);
	}

}
