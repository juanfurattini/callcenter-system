package com.almundo.callcenter.models.call;

/**
 * PhoneCall
 * 
 * This class represents the model of a phone number
 * 
 * @author juan.m.furattini
 *
 */
public class PhoneNumber {

	private final int countryCode;
	private final int areaCode;
	private final long number;

	public PhoneNumber(final int countryCode, final int areaCode, final long phoneNumber) {
		super();
		this.countryCode = countryCode;
		this.areaCode = areaCode;
		this.number = phoneNumber;
	}

	public int getCountryCode() {
		return countryCode;
	}

	public int getAreaCode() {
		return areaCode;
	}

	public long getNumber() {
		return number;
	}

	@Override
	public String toString() {
		return String.format("+%s%s%s", countryCode, areaCode, number);

	}

}
