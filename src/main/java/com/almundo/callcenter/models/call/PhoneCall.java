package com.almundo.callcenter.models.call;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.almundo.callcenter.models.employee.Employee;
import com.almundo.callcenter.types.CallStatus;

/**
 * PhoneCall
 * 
 * This class represents the model of a phone call
 * 
 * @author juan.m.furattini
 *
 */
public class PhoneCall {

	private final PhoneNumber phoneNumber;
	private Date startedAt;
	private Date finishedAt;
	private Employee answeredBy;
	private CallStatus status = CallStatus.WAITING;

	public PhoneCall(final PhoneNumber phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public PhoneNumber getPhoneNumber() {
		return phoneNumber;
	}

	public Date getStartedAt() {
		return startedAt;
	}

	public Date getFinishedAt() {
		return finishedAt;
	}

	public Employee getAnsweredBy() {
		return answeredBy;
	}

	public CallStatus getStatus() {
		return status;
	}

	/**
	 * Calculates the duration of a call in seconds
	 * 
	 * @return the duration in seconds
	 */
	public long getCallDuration() {
		return TimeUnit.MILLISECONDS.toSeconds(finishedAt.getTime() - startedAt.getTime());
	}

	/**
	 * Puts the call in hold
	 */
	public void onHold() {
		this.status = CallStatus.ON_HOLD;
		System.out.printf("Incoming call from %s on hold%n", phoneNumber);
	}

	/**
	 * Sets the calls as attended by the passed employee
	 * 
	 * @param employee
	 *            the employee that attend the call
	 */
	public void attended(Employee employee) {
		this.startedAt = new Date();
		this.answeredBy = employee;
		this.status = CallStatus.ATTENDED;
		System.out.printf("Incoming call from %s attended by %s%n", phoneNumber, answeredBy);
	}

	/**
	 * Sets the call as finished
	 */
	public void finished() {
		this.finishedAt = new Date();
		this.status = CallStatus.ENDED;
		System.out.printf("Incoming Call from %s finished by %s - Duration: %s seconds%n", phoneNumber, answeredBy,
				getCallDuration());
	}

}
