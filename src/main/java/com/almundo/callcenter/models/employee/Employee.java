package com.almundo.callcenter.models.employee;

import java.util.Observable;

import com.almundo.callcenter.models.call.PhoneCall;
import com.almundo.callcenter.observers.EmployeeActivityMonitor;
import com.almundo.callcenter.types.EmployeeStatus;
import com.almundo.callcenter.types.Priority;

/**
 * Employee
 * 
 * This class represents the base model of an employee
 * 
 * @author juan.m.furattini
 *
 */
public abstract class Employee extends Observable {

	private String userName;
	protected Priority priority;
	private EmployeeStatus status = EmployeeStatus.AVAILABLE;

	private EmployeeActivityMonitor employeeActivityMonitor = EmployeeActivityMonitor.getInstance();

	protected Employee(final String userName) {
		this.userName = userName;
		addObserver(employeeActivityMonitor);
	}

	public final String getUserName() {
		return userName;
	}

	public final Priority getPriority() {
		return priority;
	}

	public EmployeeStatus getStatus() {
		return status;
	}

	/**
	 * Change the call status, sets the user as busy and notify the activity
	 * monitor
	 * 
	 * @param call
	 *            the call to be attended
	 */
	public final void attendCall(PhoneCall call) {
		call.attended(this);
		this.status = EmployeeStatus.ATTENDING;
		setChanged();
		notifyObservers(call);
	}

	/**
	 * Change the call status, sets the user as available and notify the
	 * activity monitor
	 * 
	 * @param call
	 *            the call to be finished
	 */
	public final void finishCall(PhoneCall call) {
		call.finished();
		this.status = EmployeeStatus.AVAILABLE;
		setChanged();
		notifyObservers(call);
	}

	@Override
	public String toString() {
		return String.format("%s [%s]", userName, getClass().getSimpleName());
	}

}
