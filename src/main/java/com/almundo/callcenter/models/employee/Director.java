package com.almundo.callcenter.models.employee;

import com.almundo.callcenter.types.Priority;

/**
 * Director
 * 
 * This class represents the model of an director (subclass of employee)
 * 
 * @author juan.m.furattini
 *
 */
public class Director extends Employee {

	public Director(final String userName) {
		super(userName);
		this.priority = Priority.DIRECTOR;
	}

}
