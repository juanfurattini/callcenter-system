package com.almundo.callcenter.models.employee;

import com.almundo.callcenter.types.Priority;

/**
 * Supervisor
 * 
 * This class represents the model of an supervisor (subclass of employee)
 * 
 * @author juan.m.furattini
 *
 */
public class Supervisor extends Employee {

	public Supervisor(final String userName) {
		super(userName);
		this.priority = Priority.SUPERVISOR;
	}

}
