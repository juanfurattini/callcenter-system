package com.almundo.callcenter.models.employee;

import com.almundo.callcenter.types.Priority;

/**
 * Operator
 * 
 * This class represents the model of an operator (subclass of employee)
 * 
 * @author juan.m.furattini
 *
 */
public class Operator extends Employee {

	public Operator(final String userName) {
		super(userName);
		this.priority = Priority.OPERATOR;
	}

}
