package com.almundo.callcenter.components;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.almundo.callcenter.models.call.PhoneCall;

/**
 * CallEnqueuer
 * 
 * This class manages the queue of held calls
 * 
 * @author juan.m.furattini
 *
 */
public class CallEnqueuer {

	// Singleton instance
	private static CallEnqueuer _instance = new CallEnqueuer();

	// Attributes
	private Queue<PhoneCall> enqueuedCalls = new ConcurrentLinkedQueue<PhoneCall>();

	private CallEnqueuer() {
	}

	public static CallEnqueuer getInstance() {
		return _instance;
	}

	/**
	 * Puts the call on hold and add it to the queue
	 * 
	 * @param call
	 *            the call to hold
	 */
	public void enqueueCall(PhoneCall call) {
		call.onHold();
		enqueuedCalls.add(call);
	}

	/**
	 * Takes a call from the queue to attend
	 * 
	 * @return the call to attend
	 */
	public PhoneCall dequeueCall() {
		return enqueuedCalls.poll();
	}

	/**
	 * Checks if the queue has held calls
	 * 
	 * @return true if there are at least one held call
	 */
	public boolean areThereAnyHeldCall() {
		return !enqueuedCalls.isEmpty();
	}

}
