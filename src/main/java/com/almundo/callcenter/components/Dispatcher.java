package com.almundo.callcenter.components;

import com.almundo.callcenter.exceptions.NoEmployeeAvailableException;
import com.almundo.callcenter.models.call.PhoneCall;
import com.almundo.callcenter.models.employee.Employee;

/**
 * Dispatcher
 * 
 * This class manages the flow of calls, deriving them to an employee or enqueue
 * them if there are no free employees
 * 
 * @author juan.m.furattini
 *
 */
public class Dispatcher {

	// Singleton instance
	private static Dispatcher _instance = new Dispatcher();

	// Components
	private AvailabilityManager availabilityManager = AvailabilityManager.getInstance();
	private CallEnqueuer callEnqueuer = CallEnqueuer.getInstance();

	private Dispatcher() {
	}

	public static Dispatcher getInstance() {
		return _instance;
	}

	/**
	 * Process a call and assign it to an employee or enqueue it if there is no
	 * employee available to attend
	 * 
	 * @param call
	 *            the call to be attend
	 */
	public void dispatchCall(final PhoneCall call) {
		System.out.printf("Processing call from %s%n", call.getPhoneNumber());
		try {
			Employee attender = availabilityManager.takeNextAvailableEmployee();
			attender.attendCall(call);
		} catch (NoEmployeeAvailableException e) {
			callEnqueuer.enqueueCall(call);
		}
	}

	/**
	 * Handles the event that is fired when an employee is released
	 * 
	 * It notifies the availability manager to release the employee, takes the
	 * next enqueued call and dispatches it
	 * 
	 * @param employee
	 *            the employee to release
	 */
	public void employeeReleased(Employee employee) {
		System.out.printf("Employee %s is free again%n", employee);
		availabilityManager.releaseEmployee(employee);
		if (callEnqueuer.areThereAnyHeldCall()) {
			PhoneCall pendingCall = callEnqueuer.dequeueCall();
			dispatchCall(pendingCall);
		}
	}

}