package com.almundo.callcenter.components;

import java.util.Collection;
import java.util.Comparator;
import java.util.PriorityQueue;

import com.almundo.callcenter.exceptions.NoEmployeeAvailableException;
import com.almundo.callcenter.models.employee.Employee;

/**
 * AvailabilityManager
 * 
 * This class manages the employees availability
 * 
 * @author juan.m.furattini
 *
 */
public class AvailabilityManager {

	// Singleton instance
	private static AvailabilityManager _instance = new AvailabilityManager();

	private static final int MAX_CONCURRENT_CALLS = 10;

	// Attributes
	private PriorityQueue<Employee> availableEmployees = new PriorityQueue<Employee>(MAX_CONCURRENT_CALLS,
			new Comparator<Employee>() {
				@Override
				public int compare(Employee o1, Employee o2) {
					return o1.getPriority().getFactor().compareTo(o2.getPriority().getFactor());
				}
			});

	private AvailabilityManager() {
	}

	public static AvailabilityManager getInstance() {
		return _instance;
	}

	/**
	 * Initializes the availability manager with the passed list of employees
	 * 
	 * @param employees
	 *            a list of employees to attend the incoming calls
	 */
	public void prepareEmployees(Collection<Employee> employees) {
		// EMPLOYEES
		System.out.println("Preparing employees");
		for (Employee employee : employees) {
			availableEmployees.offer(employee);
		}
		System.out.printf("%s employees ready to attend%n", availableEmployees.size());
	}

	/**
	 * Takes the next available employee from the queue
	 * 
	 * Returns an operator, if there is no free operator it returns a
	 * supervisor, and if there is no free supervisor it returns a director
	 * 
	 * @return the next available employee to attend
	 * 
	 * @throws NoEmployeeAvailableException
	 *             if there is no available employee to attend
	 */
	public Employee takeNextAvailableEmployee() throws NoEmployeeAvailableException {
		Employee availableEmployee = availableEmployees.poll();
		if (availableEmployee == null) {
			throw new NoEmployeeAvailableException();
		}
		return availableEmployee;
	}

	/**
	 * Adds the passed released employee to the queue
	 * 
	 * @param employee
	 *            an employee to add to the queue
	 */
	public void releaseEmployee(Employee employee) {
		availableEmployees.add(employee);
	}

}
