package com.almundo.callcenter.client.caller;

import java.util.Collection;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.almundo.callcenter.CallCenter;
import com.almundo.callcenter.exceptions.NoCallsToAttendException;
import com.almundo.callcenter.exceptions.NoEmployeeAvailableException;
import com.almundo.callcenter.models.call.PhoneCall;
import com.almundo.callcenter.models.call.PhoneNumber;
import com.almundo.callcenter.models.employee.Director;
import com.almundo.callcenter.models.employee.Employee;
import com.almundo.callcenter.models.employee.Operator;
import com.almundo.callcenter.models.employee.Supervisor;

/**
 * CallerManager
 * 
 * This class manages the call simulations
 * 
 * @author juan.m.furattini
 *
 */
public class CallerManager {

	// Singleton instance
	private static CallerManager _instance = new CallerManager();

	// Attributes
	private static Queue<PhoneCall> incomingCalls = new ConcurrentLinkedQueue<PhoneCall>();

	private CallerManager() {
	}

	public static CallerManager getInstance() {
		return _instance;
	}

	/**
	 * Starts making incoming calls
	 * 
	 * @param calls
	 *            a list of incoming calls
	 * @throws NoCallsToAttendException
	 *             if there is no incoming calls to attend
	 */
	public void startDialing(Collection<PhoneCall> calls) {
		System.out.println("Initializing Client's Caller Manager");
		System.out.println("Preparing calls");
		for (PhoneCall call : calls) {
			incomingCalls.offer(call);
		}
		System.out.printf("%s calls ready to dial%n", incomingCalls.size());

		int poolSize = incomingCalls.size();
		System.out.println("Start dialing...");
		ExecutorService service = Executors.newFixedThreadPool(poolSize);
		for (int n = 0; n < poolSize; n++) {
			if (areThereAnyIncomingCall()) {
				PhoneCall call = incomingCalls.poll();
				service.submit(new Caller(call));
			}
		}
		service.shutdownNow();

	}

	/**
	 * Checks if the queue has pending calls to attend
	 * 
	 * @return true if there are at least one call to attend
	 */
	public boolean areThereAnyIncomingCall() {
		return !incomingCalls.isEmpty();
	}

	/**
	 * System entry point
	 * 
	 * @param args
	 * @throws NoCallsToAttendException
	 * @throws NoEmployeeAvailableException
	 */
	public static void main(String[] args) throws NoEmployeeAvailableException, NoCallsToAttendException {

		// Creating the call center employees
		Set<Employee> employees = new HashSet<Employee>();
		employees.add(new Operator("jfurattini"));
		employees.add(new Operator("maguilar"));
		employees.add(new Operator("mfernandez"));
		employees.add(new Supervisor("fsacchetti"));
		employees.add(new Director("nmuchaplata"));

		// Initialize the call center
		CallCenter.getInstance().initializeSystem(employees);

		// Creating the calls
		Set<PhoneCall> calls = new HashSet<PhoneCall>();
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45675422)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 46025237)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 46352155)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 49119766)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 46822112)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 49577231)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 43423235)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 43413276)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 46017913)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 46013019)));

		// Starting the caller manager
		ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(1);
		scheduledThreadPool.schedule(new Runnable() {
			@Override
			public void run() {
				CallerManager.getInstance().startDialing(calls);
			}
		}, 5, TimeUnit.SECONDS);
		scheduledThreadPool.shutdown();

	}

}
