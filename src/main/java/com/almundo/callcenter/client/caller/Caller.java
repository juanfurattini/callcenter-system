package com.almundo.callcenter.client.caller;

import com.almundo.callcenter.CallCenter;
import com.almundo.callcenter.models.call.PhoneCall;

/**
 * Caller
 * 
 * This class represents a simulation of a person who makes a call
 * 
 * @author juan.m.furattini
 *
 */
public class Caller implements Runnable {

	private PhoneCall call;

	private static CallCenter callCenter = CallCenter.getInstance();

	public Caller(PhoneCall call) {
		this.call = call;
	}

	@Override
	public void run() {
		System.out.println("Calling...");
		callCenter.processCall(call);
	}

}
