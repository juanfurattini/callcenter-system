package com.almundo.callcenter;

import java.util.Collection;
import java.util.Objects;

import com.almundo.callcenter.components.AvailabilityManager;
import com.almundo.callcenter.components.Dispatcher;
import com.almundo.callcenter.exceptions.NoEmployeeAvailableException;
import com.almundo.callcenter.models.call.PhoneCall;
import com.almundo.callcenter.models.employee.Employee;

/**
 * CallCenter
 * 
 * This is the entry point of the system
 * 
 * @author juan.m.furattini
 *
 */
public class CallCenter {

	// Singleton instance
	private static CallCenter _instance = new CallCenter();

	// Components
	private Dispatcher dispatcher = Dispatcher.getInstance();

	private CallCenter() {
	}

	public static CallCenter getInstance() {
		return _instance;
	}

	/**
	 * Initializes the system with a list of employees and a list of incoming
	 * calls
	 * 
	 * @param employees
	 *            a list of employees to attend the incoming calls
	 * @throws NoEmployeeAvailableException
	 *             if there is no employees to work
	 */
	public void initializeSystem(Collection<Employee> employees) throws NoEmployeeAvailableException {
		if (Objects.isNull(employees) || employees.isEmpty()) {
			throw new NoEmployeeAvailableException("There are no available employees to work");
		}
		System.out.println("**************************************************");
		System.out.println("*** Callcenter System by Juan Manuel Furattini ***");
		System.out.println("**************************************************");
		System.out.println("--> Initializing system");
		AvailabilityManager.getInstance().prepareEmployees(employees);
		System.out.println("**************************************************");
	}

	/**
	 * Sends the passed phone call to the dispatcher
	 * 
	 * @param call
	 *            to dispatch
	 */
	public void processCall(PhoneCall call) {
		dispatcher.dispatchCall(call);
	}

}
