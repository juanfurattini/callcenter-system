package com.almundo.callcenter.types;

/**
 * CallStatus
 * 
 * This enumeration holds the phone calls statuses
 * 
 * @author juan.m.furattini
 *
 */
public enum CallStatus {

	WAITING, ON_HOLD, ATTENDED, ENDED;

}
