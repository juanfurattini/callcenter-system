package com.almundo.callcenter.types;

/**
 * EmployeeStatus
 * 
 * This enumeration holds the employee statuses
 * 
 * @author juan.m.furattini
 *
 */
public enum EmployeeStatus {

	AVAILABLE, ATTENDING;

}
