package com.almundo.callcenter.types;

/**
 * Priority
 * 
 * This enumeration holds the employee priorities based on the employee profile
 * 
 * @author juan.m.furattini
 *
 */
public enum Priority {

	OPERATOR(1), SUPERVISOR(2), DIRECTOR(3);

	private final Integer factor;

	Priority(final int priorityValue) {
		this.factor = priorityValue;
	}

	public Integer getFactor() {
		return factor;
	}

}
