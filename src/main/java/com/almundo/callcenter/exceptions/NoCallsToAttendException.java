package com.almundo.callcenter.exceptions;

/**
 * NoCallsToAttendException
 * 
 * This class represents the exception that occurs when there is no call to
 * attend
 * 
 * @author juan.m.furattini
 *
 */
public class NoCallsToAttendException extends Exception {

	private static final long serialVersionUID = 7780510220566825344L;

	public NoCallsToAttendException() {
		super();
	}

	public NoCallsToAttendException(String message) {
		super(message);
	}

}
