package com.almundo.callcenter.exceptions;

/**
 * NoEmployeeAvailableException
 * 
 * This class represents the exception that occurs when there is no employee
 * available
 * 
 * @author juan.m.furattini
 *
 */
public class NoEmployeeAvailableException extends Exception {

	private static final long serialVersionUID = -3380983560021700512L;

	public NoEmployeeAvailableException() {
		super();
	}

	public NoEmployeeAvailableException(String message) {
		super(message);
	}

}
