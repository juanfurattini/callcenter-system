package com.almundo.callcenter.components;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import com.almundo.callcenter.exceptions.NoEmployeeAvailableException;
import com.almundo.callcenter.models.employee.Employee;
import com.almundo.callcenter.models.employee.Operator;

public class AvailabilityManagerTest {

	private static AvailabilityManager availabilityManager;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		availabilityManager = AvailabilityManager.getInstance();
	}

	@Test
	public void whenThereIsNoEmployeesAnNoEmployeeAvailableExceptionMustBeThrown() {
		availabilityManager.prepareEmployees(new ArrayList<Employee>());
		try {
			availabilityManager.takeNextAvailableEmployee();
			fail("An NoEmployeeAvailableException must be thrown");
		} catch (NoEmployeeAvailableException e) {
			assertTrue(e != null);
		}
	}

	@Test
	public void whenThereIsEmployeesAvailableTheFirstOneMustBeReturned() {
		ArrayList<Employee> employees = new ArrayList<Employee>();
		Employee operator1 = new Operator("operator1");
		employees.add(operator1);
		availabilityManager.prepareEmployees(employees);
		try {
			availabilityManager.takeNextAvailableEmployee();
		} catch (NoEmployeeAvailableException e) {
			fail("Unexpected exception");
		}
	}

	@Test
	public void whenAnEmployeeReleaseACallItMustBeAvailableAgain() {
		availabilityManager.prepareEmployees(new ArrayList<Employee>());
		availabilityManager.releaseEmployee(new Operator("operator1"));
		try {
			Employee emp = availabilityManager.takeNextAvailableEmployee();
			assertEquals("operator1", emp.getUserName());
		} catch (NoEmployeeAvailableException e) {
			fail("Unexpected exception");
		}
	}

}
