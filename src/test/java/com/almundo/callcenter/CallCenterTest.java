/**
 * 
 */
package com.almundo.callcenter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import com.almundo.callcenter.client.caller.CallerManager;
import com.almundo.callcenter.components.CallEnqueuer;
import com.almundo.callcenter.exceptions.NoEmployeeAvailableException;
import com.almundo.callcenter.models.call.PhoneCall;
import com.almundo.callcenter.models.call.PhoneNumber;
import com.almundo.callcenter.models.employee.Employee;
import com.almundo.callcenter.models.employee.Operator;

/**
 * @author juan.m.furattini
 *
 */
public class CallCenterTest {

	private static CallCenter callCenter;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		callCenter = CallCenter.getInstance();
	}

	@Test
	public void whenInitializeSystemAndThereIsNoEmployeesToWorkAnNoEmployeeAvailableExceptionMustBeThrown() {
		Set<Employee> employees = new HashSet<Employee>();
		try {
			callCenter.initializeSystem(employees);
			fail("An NoEmployeeAvailableException must be thrown");
		} catch (NoEmployeeAvailableException e) {
			assertTrue(e != null);
		}
	}

	@Test
	public void whenInitializeSystemAndEmployeesIsNullAnNoEmployeeAvailableExceptionMustBeThrown() {
		Set<Employee> employees = null;
		try {
			callCenter.initializeSystem(employees);
			fail("An NoEmployeeAvailableException must be thrown");
		} catch (NoEmployeeAvailableException e) {
			assertTrue(e != null);
		}
	}

	@Test
	public void whenInitializeSystemAfter10CallsWith10EmployeesZeroCallsMustBeEnqueued() {
		Set<Employee> employees = new HashSet<Employee>();
		employees.add(new Operator("employee1"));
		employees.add(new Operator("employee2"));
		employees.add(new Operator("employee3"));
		employees.add(new Operator("employee4"));
		employees.add(new Operator("employee5"));
		employees.add(new Operator("employee6"));
		employees.add(new Operator("employee7"));
		employees.add(new Operator("employee8"));
		employees.add(new Operator("employee9"));
		employees.add(new Operator("employee10"));

		Set<PhoneCall> calls = new HashSet<PhoneCall>();
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550001)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550002)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550003)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550004)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550005)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550006)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550007)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550008)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550009)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550010)));

		try {
			callCenter.initializeSystem(employees);
		} catch (NoEmployeeAvailableException e) {
			fail("Unexpected exception");
		}

		assertFalse(CallEnqueuer.getInstance().areThereAnyHeldCall());
		assertTrue(CallerManager.getInstance().areThereAnyIncomingCall());

		CallerManager.getInstance().startDialing(calls);

		assertFalse(CallEnqueuer.getInstance().areThereAnyHeldCall());
		assertFalse(CallerManager.getInstance().areThereAnyIncomingCall());

	}

	@Test
	public void whenInitializeSystemAfter10CallsWith5EmployeesZeroCallsMustBeEnqueued() {
		Set<Employee> employees = new HashSet<Employee>();
		employees.add(new Operator("employee1"));
		employees.add(new Operator("employee2"));
		employees.add(new Operator("employee3"));
		employees.add(new Operator("employee4"));
		employees.add(new Operator("employee5"));

		Set<PhoneCall> calls = new HashSet<PhoneCall>();
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550001)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550002)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550003)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550004)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550005)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550006)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550007)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550008)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550009)));
		calls.add(new PhoneCall(new PhoneNumber(54, 11, 45550010)));

		try {
			callCenter.initializeSystem(employees);
		} catch (NoEmployeeAvailableException e) {
			fail("Unexpected exception");
		}

		assertFalse(CallEnqueuer.getInstance().areThereAnyHeldCall());
		assertTrue(CallerManager.getInstance().areThereAnyIncomingCall());

		CallerManager.getInstance().startDialing(calls);

		assertFalse(CallEnqueuer.getInstance().areThereAnyHeldCall());
		assertFalse(CallerManager.getInstance().areThereAnyIncomingCall());

	}

}
